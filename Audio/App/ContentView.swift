//
//  ContentView.swift
//  Audio
//
//  Created by Amr Hossam on 16/05/2022.
//

import SwiftUI

struct ContentView: View {
    
    
    var body: some View {
        ZStack {
            HomeView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
