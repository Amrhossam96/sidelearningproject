//
//  AudioApp.swift
//  Audio
//
//  Created by Amr Hossam on 16/05/2022.
//

import SwiftUI

@main
struct AudioApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
