//
//  AuthButton.swift
//  Audio
//
//  Created by Amr Hossam on 16/05/2022.
//

import SwiftUI

struct AuthButton: View {
    // MARK: - PROPERTIES
    let buttonLabel: String
    let action: () -> Void
    
    var body: some View {
        Button {
            action()
        } label: {
            Text(buttonLabel)
                .frame(maxWidth: .infinity, maxHeight: 50)
                .background(Color.primaryColor)
                .foregroundColor(.white)
                .cornerRadius(10)

        }

    }
}

struct AuthButton_Previews: PreviewProvider {
    static var previews: some View {
        AuthButton(buttonLabel: "Sign In", action: {})
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
