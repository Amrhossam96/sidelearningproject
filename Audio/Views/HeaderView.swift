//
//  HeaderView.swift
//  Audio
//
//  Created by Amr Hossam on 17/05/2022.
//

import SwiftUI

struct HeaderView: View {
    let name: String
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            Text("Hi, \(name)")
                .font(.system(size: 16))
            Text("What are you looking for\ntoday")
                .font(.system(size: 24))
                .bold()
        }
    }
}


struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderView(name: "Andrea")
            .previewLayout(.sizeThatFits)
    }
}
