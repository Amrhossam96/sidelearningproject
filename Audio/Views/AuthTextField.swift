//
//  AuthTextField.swift
//  Audio
//
//  Created by Amr Hossam on 16/05/2022.
//

import SwiftUI

struct AuthTextField: View {
    // MARK: - PROPERTIES
    @Binding var text: String
    let placeholder: String
    let iconName: String
    
    // MARK: - BODY
    var body: some View {
        HStack {
            Image(iconName)
            TextField(placeholder, text: $text)
        } //: HSTACK
        .padding()
        .frame(height: 50)
        .background(.white)
        .cornerRadius(10)
        
    }
}

// MARK: - PREVIEW
struct AuthTextField_Previews: PreviewProvider {
    static var previews: some View {
        AuthTextField(text: .constant(""), placeholder: "Email", iconName: "mail")
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
