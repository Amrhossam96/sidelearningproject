//
//  BannerView.swift
//  Audio
//
//  Created by Amr Hossam on 17/05/2022.
//

import SwiftUI

struct BannerView: View {
    let productName: String
    var body: some View {
        HStack(spacing: 10) {
            VStack(alignment: .leading) {
                Text(productName)
                    .font((.system(size: 22)))
                    .bold()
                Button {
                    print("Pressed")
                } label: {
                    HStack {
                        Text("Shop now")
                        Image(systemName: "arrow.right")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 20, height: 20, alignment: .center)
                    }
                    .foregroundColor(Color.primaryColor)
                }
            } //: VSTACK
            
            VStack {
                Image("image5")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 117, height: 135, alignment: .center)
            } //: VSTACK
            .padding(.trailing, 24)
        } //: HSTACK
        .padding(.vertical, 21.5)
        .padding(.horizontal, 25)
        .background(Color.white.clipShape(RoundedRectangle(cornerRadius: 20)))
        
    }
}

struct BannerView_Previews: PreviewProvider {
    static var previews: some View {
        BannerView(productName: "iPod")
            .previewLayout(.sizeThatFits)
        //            .padding()
    }
}
