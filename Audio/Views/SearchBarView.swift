//
//  SearchBarView.swift
//  Audio
//
//  Created by Amr Hossam on 17/05/2022.
//

import SwiftUI

struct SearchBarView: View {
    

    let action: () -> Void
    var body: some View {
        HStack {
            Image(systemName: "magnifyingglass")
                .resizable()
                .scaledToFit()
                .frame(width: 20, height: 20, alignment: .center)
                .foregroundColor(.gray)
                .padding(.leading, 15)
                .padding(.trailing, 12)

            Button {
                action()
            } label: {
                HStack {
                    Text("Search Headphone")
                        .foregroundColor(.gray)
                    Spacer()
                }
                    
            }

        }
        .padding(.vertical, 15)
        .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(.gray, lineWidth: 1)
            )
    }
}

struct SearchBarView_Previews: PreviewProvider {
    static var previews: some View {
        SearchBarView(action: {})
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
