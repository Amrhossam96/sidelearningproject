//
//  ThirdPartyAuthView.swift
//  Audio
//
//  Created by Amr Hossam on 17/05/2022.
//

import SwiftUI

struct ThirdPartyAuthButtonView: View {
    
    let providerImage: String
    let action: () -> Void

    var body: some View {
        Button {
            action()
        } label: {
            Image(providerImage)
                .frame(width: 52, height: 52)
                .background(.white)
                .cornerRadius(9.4)
        }

    }
}

struct ThirdPartyAuthView_Previews: PreviewProvider {
    static var previews: some View {
        ThirdPartyAuthButtonView(providerImage: "googleIcon", action: {})
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
