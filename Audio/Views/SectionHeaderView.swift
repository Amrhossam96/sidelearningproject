//
//  SectionHeaderView.swift
//  Audio
//
//  Created by Amr Hossam on 17/05/2022.
//

import SwiftUI

struct SectionHeaderView: View {
    let sectionTitle: String
    let action: () -> Void
    var body: some View {
        HStack {
            Text(sectionTitle)
            Spacer()
            Button {
                action()
            } label: {
                Text("See All")
                    .foregroundColor(.gray)
            }

        }
    }
}

struct SectionHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        SectionHeaderView(sectionTitle: "Featured Products", action: {})
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
