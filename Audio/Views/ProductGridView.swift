//
//  ProductGridView.swift
//  Audio
//
//  Created by Amr Hossam on 17/05/2022.
//

import SwiftUI

struct ProductGridView: View {
    var body: some View {
        VStack(alignment: .leading) {
            Image("image5")
                .resizable()
                .scaledToFit()
                .frame(width: 135, height: 125, alignment: .center)
            VStack(alignment: .leading) {
                Text("TMA-2 HD Wireless")
                    .font((.system(size: 14)))
                Text("USD 350")
                    .font(.system(size: 12))
                    .bold()
                    .padding(.bottom, 15)
            }
        }
        .frame(width: 155)
        .background(.white)
        .clipShape(RoundedRectangle(cornerRadius: 20))
    }
}

struct ProductGridView_Previews: PreviewProvider {
    static var previews: some View {
        ProductGridView()
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
