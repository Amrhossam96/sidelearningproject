//
//  NavigationBarView.swift
//  Audio
//
//  Created by Amr Hossam on 17/05/2022.
//

import SwiftUI

struct NavigationBarView: View {
    var body: some View {
        HStack {
            Button {
                print("Side menu")
            } label: {
                Image("menu-variant")
            } //: BUTTON
            Spacer()
            HStack {
                Image("audioIcon")
                Text("Audio")
                    .font(.system(size: 19))
                    .bold()
            } //: HSTACK
            Spacer()
            Button {
                print("person")
            } label: {
                Image("avatar1")
                    .resizable()
                    .scaledToFill()
                    .frame(width: 35, height: 35, alignment: .center)
                    .clipShape(Circle())
            } //: BUTTON
        } //: HSTACK
    }
}

struct NavigationBarView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationBarView()
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
