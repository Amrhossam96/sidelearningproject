//
//  HomeView.swift
//  Audio
//
//  Created by Amr Hossam on 17/05/2022.
//

import SwiftUI

struct HomeView: View {
    // MARK: - PROPERTIES
    private var categories: [String] = ["Headphone", "Headband", "Earpads", "Cables", "Charges"]
    private var products: [String] = ["TMA-2 Modular Headphone", "TMA-3 Modular Headphone", "TMA-4 Modular Headphone", "TMA-5 Modular Headphone", "TMA-6 Modular Headphone", "TMA-8 Modular Headphone"]
    @State var searchText: String = ""

    // MARK: - BODY
    var body: some View {
        
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                NavigationBarView()
                    .padding(.horizontal, 25)

                HStack {
                    HeaderView(name: "Andrea")
                        .padding(.top, 29)
                    Spacer()
                } //: HSTACK
                .padding(.horizontal, 25)

                NavigationLink {
                    
                } label: {
                    SearchBarView {
                        print("Go to search")
                    }
                    .padding(.horizontal, 25)
                }


                
                VStack {
                    ScrollView(.horizontal, showsIndicators: false) {
                        LazyHStack(spacing: 11) {
                            ForEach(categories, id: \.self) {
                                Text($0)
                                    .padding(.horizontal, 15)
                                    .font(.system(size: 14))
                                    .padding(8)
                                    .background($0 == "Headphone" ? Color.primaryColor : nil)
                                    .foregroundColor($0 == "Headphone" ? .white : .gray)
                                    .clipShape(Capsule())
                            }
                        } //: LAZYHSTACK
                        .padding(.top, 32)
                    } //: SCROLL
                    .padding(.horizontal)
                    
                    ScrollView(.horizontal, showsIndicators: false) {
                        LazyHStack(spacing: 15) {
                            ForEach(products, id: \.self) {
                                BannerView(productName: $0)
                                    .frame(width: 326)
                            }
                        }
                    } //: SCROLL
                    .padding(.top, 20)
                    
                    SectionHeaderView(sectionTitle: "Featured Products") {
                        print("Pressed")
                    }
                    .padding(.horizontal, 24)
                    .padding(.top, 20)
                    
                    ScrollView(.horizontal, showsIndicators: false) {
                        LazyHStack {
                            ForEach(0..<5) { _ in
                                ProductGridView()
                                    .padding(.bottom, 15)
                            }
                        }
                    }
                    Spacer()
                    
                } //: VSTACK
                .background {
                    Color.lightGrayColor.clipShape(CustomShape(radius: 35))
                }
                .padding(.top, 25)
                .navigationBarHidden(true)
            }
        } //: SCROLL
        
    }
}
    // MARK: - PREVIEW
struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

