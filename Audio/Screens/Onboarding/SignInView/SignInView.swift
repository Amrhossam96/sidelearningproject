//
//  SignInView.swift
//  Audio
//
//  Created by Amr Hossam on 16/05/2022.
//

import SwiftUI




struct SignInView: View {
    
    @State private var email: String = ""
    @State private var password: String = ""
    
    var body: some View {
        ZStack {
            Image("signInBackground")
                .resizable()
                .scaledToFill()
                .ignoresSafeArea()
            
            VStack() {
                VStack(spacing: 7) {
                    
                    Text("Audio")
                        .font(.system(size: 51))
                        .foregroundColor(.white)
                        .fontWeight(.bold)
                        .tracking(1.5)

                        
                    
                    Text("It's modular and designed to last")
                        .foregroundColor(.white)
                        .font(.system(size: 14))
                        .fontWeight(.bold)
                }
                .frame(width: 227, height: 94)
                .padding(.top, 106)
                Text(email)
                VStack(spacing: 32) {
                    VStack(spacing: 20) {
                        AuthTextField(text: $email, placeholder: "Email", iconName: "mail")
                        AuthTextField(text: $password, placeholder: "Password", iconName: "lock")
                        Button {
                            print("Pressed")
                        } label: {
                            Text("Forgot Password")
                                .bold()
                                .font(.system(size: 14))
                                .foregroundColor(.white)
                        }
                    }
                    
                    VStack(spacing: 24) {
                        AuthButton(buttonLabel: "Sign In") {
                            print("Sign in tapped")
                        }
                        HStack {
                            Text("Don't have any account?")
                                .foregroundColor(.white)
                            Button {
                                print("Go to sign up")
                            } label: {
                                Text("Sign Up here")
                                    .foregroundColor(Color.primaryColor)
                                    .underline()
                            }

                        }
                    }
                }
                .padding(.horizontal, 25)
                .padding(.top, 247)
                Spacer(minLength: 79)

            }

            
        }
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
    }
}
