//
//  SignUpView.swift
//  Audio
//
//  Created by Amr Hossam on 16/05/2022.
//

import SwiftUI

struct SignUpView: View {
    @State private var email: String = ""
    @State private var password: String = ""
    
    var body: some View {
        ZStack {
            Image("signInBackground")
                .resizable()
                .scaledToFill()
                .ignoresSafeArea()
            
            VStack() {
                VStack(spacing: 7) {
                    
                    Text("Audio")
                        .font(.system(size: 51))
                        .foregroundColor(.white)
                        .fontWeight(.bold)
                        .tracking(1.5)
                        
                    
                    Text("It's modular and designed to last")
                        .foregroundColor(.white)
                        .font(.system(size: 14))
                        .fontWeight(.bold)
                }
                .frame(width: 227, height: 94)
                .padding(.top, 106)
                Text(email)
                VStack(spacing: 32) {
                    VStack(spacing: 20) {
                        AuthTextField(text: $email, placeholder: "Email", iconName: "mail")
                        AuthTextField(text: $password, placeholder: "Password", iconName: "lock")
                        Button {
                            print("Pressed")
                        } label: {
                            Text("Forgot Password")
                                .bold()
                                .font(.system(size: 14))
                                .foregroundColor(.white)
                        }
                    }
                    
                    VStack(spacing: 42) {
                        AuthButton(buttonLabel: "Sign Up") {
                            print("Sign in tapped")
                        }
                        VStack {
                            HStack(spacing: 15) {
                                ThirdPartyAuthButtonView(providerImage: "appleIcon") {
                                    print("Apple pressed")
                                }
                                
                                ThirdPartyAuthButtonView(providerImage: "facebookIcon") {
                                    print("facebook pressed")
                                }
                                
                                ThirdPartyAuthButtonView(providerImage: "googleIcon") {
                                    print("Google pressed")
                                }
                            }
                            HStack {
                                Text("Don't have any account?")
                                    .foregroundColor(.white)
                                Button {
                                    print("Go to sign up")
                                } label: {
                                    Text("Sign Up here")
                                        .foregroundColor(Color.primaryColor)
                                        .underline()
                                }

                            }
                        }
                    }
                }
                .padding(.horizontal, 25)
                .padding(.top, 164)
                Spacer(minLength: 79)

            }

            
        }
    }
}

struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpView()
    }
}
