//
//  CustomShape.swift
//  Audio
//
//  Created by Amr Hossam on 17/05/2022.
//

import SwiftUI

struct CustomShape: Shape {
    let radius: CGFloat
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: [.topLeft, .topRight],
                                cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

struct CustomShape_Previews: PreviewProvider {
    static var previews: some View {
        CustomShape(radius: 25)
            .previewLayout(.fixed(width: 428, height: 120))
            .padding()
    }
}
