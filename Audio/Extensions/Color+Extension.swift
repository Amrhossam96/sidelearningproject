//
//  Color+Extension.swift
//  Audio
//
//  Created by Amr Hossam on 16/05/2022.
//

import SwiftUI


extension Color {
    static var primaryColor: Color {
        Color("primaryColor")
    }
    
    static var lightGrayColor: Color {
        Color("lightGray")
    }
}


